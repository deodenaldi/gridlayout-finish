package com.example.bigproject;

import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity_Quiz extends AppCompatActivity {
    EditText ed1;
    TextView tv1, tv2, tv3;
    RadioButton a, b, c, d;
    Button bt;
    RadioGroup rg;
    int q, s;
    ImageView image_smile, image_sad, image_angry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_quiz);
        ed1 = (EditText) findViewById(R.id.name);
        tv1 = (TextView) findViewById(R.id.ques);
        tv2 = (TextView) findViewById(R.id.response);
        tv3 = (TextView) findViewById(R.id.score);
        rg = (RadioGroup) findViewById(R.id.optionGroup);
        a = (RadioButton) findViewById(R.id.option1);
        b = (RadioButton) findViewById(R.id.option2);
        c = (RadioButton) findViewById(R.id.option3);
        d = (RadioButton) findViewById(R.id.option4);
        bt = (Button) findViewById(R.id.next);
        image_smile = (ImageView) findViewById(R.id.img_smile);
        image_angry = (ImageView) findViewById(R.id.img_angry);
        image_sad = (ImageView) findViewById(R.id.img_sad);
        q = 0;
        s = 0;

    }

    public void quiz(View v){
        switch (q){
            case 0:
            {
                ed1.setVisibility(View.GONE);
                rg.setVisibility(View.VISIBLE);
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                tv2.setText("");
                tv3.setText("");
                a.setEnabled(true);
                b.setEnabled(true);
                c.setEnabled(true);
                d.setEnabled(true);
                ed1.setEnabled(true);
                bt.setText("Next");
                s=0;

                tv1.setText("1. Dalam menyusun suatu program,langkah pertama yang harus di lakkukan adalah ?");
                a.setText("Membut Program");
                b.setText("Membuat Algoritma");
                c.setText("membeli Komputer");
                d.setText("Mempelajari Program");
                q = 1;
                break;
            }
            case 1: {
                ed1.setEnabled(false);
                tv1.setText("2. Sebuah prosedur langkah demi langkah yang pasti untuk menyelesaikan sebuah   masalah di sebut ?");
                a.setText("Proses");
                b.setText("Program");
                c.setText("Algoritma");
                d.setText("Diagram");

                if (b.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!b.isChecked()) {
                    SalahJawab();
                }
                q = 2;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 2: {
                tv1.setText("3. Pseudocode yang di gunakan pada penulisan algoritma berupa ?");
                a.setText("Bahasa Inggris");
                b.setText("Bahasa Puitis");
                c.setText("Bahasa Pemograman");
                d.setText("Bahasa Mesin");
                if (c.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!c.isChecked()) {
                    SalahJawab();
                }
                q = 3;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 3: {

                tv1.setText("4. Pada pembuatan program komputer, algoritma dibuat?");
                a.setText("Sebelum pembuatan program");
                b.setText("Pada Saat program dibuat");
                c.setText("Sesudah pembuatan program");
                d.setText("Pada saat program dijalankan");
                if (c.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!c.isChecked()) {
                    SalahJawab();
                }
                q = 4;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 4: {
                tv1.setText("5. Tahapan dalam menyelesaikan suatu masalah adalah ?");
                a.setText("Masalah-Pseudocode-Flowchart-Program-Eksekusi-Hasil");
                b.setText("Masalah-Algoritma-Flowchart-Program-Eksekusi-Hasil");
                c.setText("Masalah-Model-Algoritma-Eksekusi-Hasil");
                d.setText("Masalah-Model-Algoritma-Program-Eksekusi-hasil ");
                if (a.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!a.isChecked()) {
                    SalahJawab();
                }
                q = 5;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 5: {
                tv1.setText("6. Diketahui bahwa kantong P kosong. Kantong Q berissi 10 buah kelereng dan kantong R berisi 15 kelereng. Apabila yang terbawa hanya sebuah kantong dan di katakan BUKAN kantong P yang terbawa, Maka jumlah kelereng yang terbawa adalah?");
                a.setText("10");
                b.setText("15");
                c.setText("10 atau 15");
                d.setText("Kosong");

                if (c.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!c.isChecked()) {
                    SalahJawab();
                }
                q = 6;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 6: {
                tv1.setText("7. Diberikan algoritma : Apabila warna merah maka jadi hijau. Apabila warna hijau maka jadi putih, selain warna merah dan hijau maka jadi ungu. Jika kondisi input warna adalah hitam, maka warna jadi ?");
                a.setText("Merah");
                b.setText("Ungu");
                c.setText("Hijau");
                d.setText("putih");

                if (c.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!c.isChecked()) {
                    SalahJawab();
                }
                q = 7;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 7: {
                tv1.setText("8. Instruksi P=Q akan mengakibatkan nilai P=nilaiQ,dan nilai Q menjadi ?");
                a.setText("Menjadi Sembarang Nilai");
                b.setText("Menjadi hampa");
                c.setText("P tetap");
                d.setText("Menjadi 10");

                if (b.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!b.isChecked()) {
                    SalahJawab();
                }
                q = 8;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 8: {
                tv1.setText("9. Apabila a=5, b=10, maka jika di berikan instruksi a=b; b=a akan mengakibatkan ?");
                a.setText("a=0 , b=5");
                b.setText("a=10 , b=5");
                c.setText("a=b");
                d.setText("a=10 , b=10");

                if (c.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!c.isChecked()) {
                    SalahJawab();
                }
                q = 9;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 9: {
                tv1.setText("10. Di berikan algoritma P=10; P=P+5; Q=P. Nilai P dan Q masing-masing adalah ?");
                a.setText("15 dan 0");
                b.setText("15 dan 15");
                c.setText("0 dan 15");
                d.setText("10 dan 15");

                if (d.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!d.isChecked()) {
                    SalahJawab();
                }
                q = 10;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }

            case 10: {
                a.setEnabled(false);
                b.setEnabled(false);
                c.setEnabled(false);
                d.setEnabled(false);
                if (b.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!b.isChecked()) {
                    SalahJawab();
                }
                tv3.setText(ed1.getText() + "'s final score is " + s);
                bt.setText("Restart");
                q = 0;
                break;
            }
        }
    }


    public void JawabBenar() {
        tv2.setText("RIGHT ANSWER !!");
        s = s + 50;
        image_smile.setVisibility(View.VISIBLE);
        image_smile.postDelayed(new Runnable() {
            @Override
            public void run() {
                image_smile.setVisibility(View.INVISIBLE);
            }
        }, 1000);
    }

    public void TidakDiJawab() {
        tv2.setText("You Are not Answerd");
        s = s + 0;
        image_angry.setVisibility(View.VISIBLE);
        image_angry.postDelayed(new Runnable() {
            @Override
            public void run() {
                image_angry.setVisibility(View.INVISIBLE);
            }
        }, 1000);
    }

    public void SalahJawab() {
        tv2.setText("SORRY.. You Wrong Answer ㅠ.ㅠ");
        s = s - 10;
        image_sad.setVisibility(View.VISIBLE);
        image_sad.postDelayed(new Runnable() {
            @Override
            public void run() {
                image_sad.setVisibility(View.INVISIBLE);
            }
        }, 1000);
    }
}
